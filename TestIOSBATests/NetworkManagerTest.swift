//
//  NetworkManagerTest.swift
//  TestIOSBATests
//
//  Created by Manuel on 18/10/21.
//

import XCTest
@testable import TestIOSBA

class NetworkManagerTest: XCTestCase {
    
    let networkManager = NetworkManager()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    /*
     * Revisamos que la consulta de shows no regrese nil
     */
    func testNilRetriveShows() throws {
        
        let expectation = XCTestExpectation(description: "Obteniendo datos de los shows")
        
        networkManager.getTvShows() { shows, error in
            XCTAssertNotNil(shows)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5)
    }
    
    /*
     * Revisamos que la consulta de shows regrese resultados
     */
    func testRetriveShows() throws {
        
        let expectation = XCTestExpectation(description: "Obteniendo datos de los shows")
        
        networkManager.getTvShows() { shows, error in
            XCTAssertTrue(shows?.count ?? 0  > 0)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5)
    }
    
    /*
     * Revisamos que la consulta de shows regrese un array de datos de tipo shows
     */
    func testTypeRetriveShows() throws {
        
        let expectation = XCTestExpectation(description: "Obteniendo datos de los shows")
        
        networkManager.getTvShows() { shows, error in
            XCTAssertTrue( shows! is [Show]  )
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5)
    }

}
