//
//  DetailShowViewModel.swift
//  TestIOSBA
//
//  Created by Manuel on 19/10/21.
//

import Foundation
import CoreData

class DetailShowViewModel {
 
    var detailShowModel = DetailShowModel()
    
    func setShow (show: Show) {
        detailShowModel.show = show
    }
    
    func getShow () -> Show? {
        return detailShowModel.show
    }
    
    func getIMDB () -> String {
        
        var buttonHidden = false
        
        if let _imdb = detailShowModel.show?.externals.imdb {
            detailShowModel.imdb += _imdb
        } else {
            buttonHidden = true
        }
        
        return detailShowModel.imdb
    }
    
    func getButtonIMDBHidden () -> Bool {
        
        var buttonHidden = false
        
        if detailShowModel.show?.externals.imdb == nil{
            buttonHidden = true
        }
        
        return buttonHidden
    }
    
    func getLabelButton () -> String {
        return isFavorite() ? "Delete" : "Favorite"
    }
    
    func doFavoriteButtonAction() {
        
        // ESTO NO DEEBRIA IR AQUI, PERO POR FATA DE TIEMPO ...
        
        if isFavorite() {
            deleteFavoriteShow(show: getShow()!)
        } else {
            saveFavoriteShow(show: getShow()!)
        }
        
    }
    
    private func isFavorite() -> Bool{
        
        let context = Connection().context()
        
        let fetchRequestShow:NSFetchRequest<CDShows> = CDShows.fetchRequest()
        
        guard let show = self.detailShowModel.show else {
            return false
        }
        
        let id = NSNumber(value: Int(show.id))
        
        fetchRequestShow.predicate = NSPredicate(format: "id = %@", id as CVarArg)
        
        do {
            let shows = try context.fetch(fetchRequestShow)
            
            if shows.count > 0 {
                return true
            }
            
        } catch let error {
            print("Error al obtener los shows: \(error.localizedDescription)")
        }
        
        return false
    }
    
    private func deleteFavoriteShow(show: Show) {
        // ESTO NO DEBERIA IR AQUI, FATO TIEMPO
        let context = Connection().context()
        
        let fetchRequestShow:NSFetchRequest<CDShows> = CDShows.fetchRequest()
        
        let id = NSNumber(value: Int(show.id))
        
        fetchRequestShow.predicate = NSPredicate(format: "id = %@", id as CVarArg)
        
        let cdShows = try! context.fetch(fetchRequestShow)
        
        cdShows.forEach({ (cdShow) in
            context.delete(cdShow)
        })

        do {
            try context.save()
        } catch {
            // Do something... fatalerror
            print("Error al eliminar el show \(error.localizedDescription)")
        }
        
    }
    
    private func saveFavoriteShow (show: Show) {
        
        print("llegue a saveFavoriteShow: \(show)")
        
        let context = Connection().context()
        
        let entityShow = NSEntityDescription.insertNewObject(forEntityName: "CDShows", into: context) as! CDShows
        
        entityShow.id = Int64(show.id)
        entityShow.name = show.name
        entityShow.url = show.url
        entityShow.summary = show.summary
        
        do {
            try context.save()
        } catch let error {
            print("No guardo el show: \(error.localizedDescription)")
        }
        
        print("Se guardo el show")
        
        // Guardamos la imagen
        let entityImage = NSEntityDescription.insertNewObject(forEntityName: "CDImages", into: context) as! CDImages
        
        entityImage.show_id = Int64(show.id)
        entityImage.medium = show.image.medium
        entityImage.original = show.image.original
        
        //entityShow.mutableSetValue(forKey: "relationshipToImages").add(entityImage)
        
        do {
            try context.save()
        } catch let error {
            print("No guardo la imagen: \(error.localizedDescription)")
        }
        
        print("Se guardo la imagen: \(entityImage)")
        
        // Guardamos el external
        let entityExternal = NSEntityDescription.insertNewObject(forEntityName: "CDExternals", into: context) as! CDExternals
        
        entityExternal.show_id = Int64(show.id)
        
        let _imdb = show.externals.imdb
        
        if _imdb != nil {
            entityExternal.imdb = _imdb
        }
        
        //entityShow.mutableSetValue(forKey: "relationshipToExternals").add(entityExternal)
        
        do {
            try context.save()
        } catch let error {
            print("No guardo el external: \(error.localizedDescription)")
        }
        
        print("Se guardo el external")
        
    }
    
}
