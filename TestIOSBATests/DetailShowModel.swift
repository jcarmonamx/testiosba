//
//  DetailShowModel.swift
//  TestIOSBA
//
//  Created by Manuel on 19/10/21.
//

import Foundation

struct DetailShowModel {
    var imdb = "https://api.tvmaze.com/lookup/shows?imdb="
    var show:Show?
}
