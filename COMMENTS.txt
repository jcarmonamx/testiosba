
Se empleo la arquitectura MVVM por que en este caso se requiere tener todas las partes del proyecto bien definidas y descopladas, ya que estamos hablando de manipular datos locales que se generan a partir de un API, el patron MVVM nos guía sobre cómo distribuir las responsabilidades entre las clases, mientras se mantiene controlado el numero de responsabilidades por clase.

Se empleo la librería Kingfisher por que permite manipular y cachear imágenes muy fácilmente, y la app se siente muy fluida.

La parte que me gustaría terminar es la parte de persistencia, intente implementar el patron de repositorios, pero por el tiempo no lo logre.

Tuve problemas con la persistencia, ya que solo he usado Realm e intente usar CoreData y el patron de repositorios, pero no lo logre, la lista de shows la obtiene de la llamada al API y en local solo se guardan los shows favoritos, fuera de eso creo que cumplí con los demás puntos.

Si tuviera mas tiempo intentaría implementar el patron de repositorios con dao, para que siempre cargue los datos de local y background se realiza una petición al api, que cuando regresa se actualiza la información de la BD y un observador notifica a la interfaz gráfica que hubo un cambio