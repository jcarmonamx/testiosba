//
//  HTTPMethod.swift
//  TestIOSBA
//
//  Created by Manuel on 16/10/21.
//

import Foundation

public enum HTTPMethod:String {
    case get    = "GET"
    case post   = "POST"
    case put    = "PUT"
    case path   = "PATH"
    case delete = "DELETE"
}
