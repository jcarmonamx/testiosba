//
//  TvMazeEndPoint.swift
//  TestIOSBA
//
//  Created by Manuel on 16/10/21.
//

import Foundation

enum NetworkEnvironment {
    case qa
    case production
    case staging
}

public enum TvMazeApi {
    case shows
    case detail(id:Int)
}

extension TvMazeApi: EndPointType {
    
    var environmentBaseURL : String {
        switch NetworkManager.environment {
        case .production: return "https://api.tvmaze.com/"
        case .qa: return "https://api.tvmaze.com/"
        case .staging: return "https://api.tvmaze.com/"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("No se pudo configurar baseURL.")}
        return url
    }
    
    var path: String {
        switch self {
        case .shows:
            return "shows"
        case .detail(let id):
            return "shows/\(id)"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        case .detail(let page):
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["page":page])
        default:
            return .request
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}


