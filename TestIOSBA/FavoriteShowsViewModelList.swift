//
//  FavoriteShowsViewModelList.swift
//  TestIOSBA
//
//  Created by Manuel on 18/10/21.
//

import Foundation
import CoreData

class FavoriteShowsViewModelList {
    
    // Modelo
    private var model = FavoriteShowModel()
    
    // Capa de datos
    var networkManager: NetworkManager! = NetworkManager()
    
    // Mecanismo de enlace
    var refreshData = { () -> () in }
    
    // Fuente de datos
    var dataArray: [Show] = [] {
        didSet {
            refreshData()
        }
    }
    
    func retriveLocalFavoriteShows () {
        
        // ESTO NO DEBERIA IR AQUI, FATO TIEMPO
        
        let context = Connection().context()
        
        let fetchRequestShows:NSFetchRequest<CDShows> = CDShows.fetchRequest()
        
        var cdShows : [CDShows] = []
        var shows = [Show]()
        
        do {
            cdShows = try context.fetch(fetchRequestShows)
        } catch let error {
            print("Erro al obtener los shows: \(error.localizedDescription)")
        }
        
        print("Entrando a consultar localmente los favoritos")
        
        cdShows.forEach({ (cdShow) in
            let fetchRequestImage:NSFetchRequest<CDImages> = CDImages.fetchRequest()
            
            let id = NSNumber(value: Int(cdShow.id))
            
            fetchRequestImage.predicate = NSPredicate(format: "show_id = %@", id as CVarArg)

            
            var image:Image?
            
            do {
                let cdImage = try context.fetch(fetchRequestImage).first
                image = Image(show_id: Int(cdShow.id), medium: cdImage!.medium ?? "", original: cdImage!.original ?? "")
            } catch let error {
                print("Erro al obtener la imagen: \(error.localizedDescription)")
            }
            
            let fetchRequestExternal:NSFetchRequest<CDExternals> = CDExternals.fetchRequest()
            
            fetchRequestExternal.predicate = NSPredicate(format: "show_id = %@", id as CVarArg)
            
            var external:Externals?
            
            do {
                let cdExternal = try context.fetch(fetchRequestExternal).first
                external = Externals(show_id: Int(cdShow.id), tvrage: Int(cdExternal!.tvrage), thetvdb: Int(cdExternal!.thetvdb), imdb: cdExternal!.imdb)
            } catch let error {
                print("Error al obtener el external: \(error.localizedDescription)")
            }
            
            let show:Show = Show(id: Int(cdShow.id), name: cdShow.name ?? "", url: cdShow.url ?? "", summary: cdShow.summary ?? "", externals: external!, image: image!)
            
            shows.append(show)
        })
         
        self.dataArray = shows
    }
    
    func getSelectetShow() -> Show? {
        return model.selectedShow
    }
    
    func setSelectetShow(show:Show) {
        model.selectedShow = show
    }
    
    func deleteFavoriteShow(row: Int) {
        
        // ESTO NO DEBERIA IR AQUI, FATO TIEMPO
        let context = Connection().context()
        
        let fetchRequestShow:NSFetchRequest<CDShows> = CDShows.fetchRequest()
        
        let show = self.dataArray[row]
        
        let id = NSNumber(value: Int(show.id))
        
        fetchRequestShow.predicate = NSPredicate(format: "id = %@", id as CVarArg)
        
        let cdShows = try! context.fetch(fetchRequestShow)
        
        cdShows.forEach({ (cdShow) in
            context.delete(cdShow)
        })

        do {
            try context.save()
        } catch {
            // Do something... fatalerror
            print("Error al eliminar el show \(error.localizedDescription)")
        }
        
        retriveLocalFavoriteShows ()
    }
    
    
}
