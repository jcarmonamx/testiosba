//
//  FavoriteShowsCustomCell.swift
//  TestIOSBA
//
//  Created by Manuel on 18/10/21.
//

import UIKit

protocol FavoriteShowsCustomCellDelegator {
    func callSegueFromCell(show:Show)
}

class FavoriteShowsCustomCell: UITableViewCell {
    
    var delegate:FavoriteShowsCustomCellDelegator!
    
    /*
     * MARK: Variables
     */
    
    var show:Show?
    
    /*
     * MARK: Outlets
     */
    
    @IBOutlet weak var imageShow: UIImageView!
    @IBOutlet weak var titleShow: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(seleccion)))
    }

}

/*
 * MARK: Funciones propias
 */
extension FavoriteShowsCustomCell {
    // Al seleccionar un row
    @objc func seleccion () {
        guard let _show =  self.show, let _ = self.delegate else { return }
        self.delegate.callSegueFromCell(show: _show)
    }
}
