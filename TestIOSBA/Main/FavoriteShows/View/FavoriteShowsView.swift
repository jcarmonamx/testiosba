//
//  FavoriteShowsView.swift
//  TestIOSBA
//
//  Created by Manuel on 17/10/21.
//

import UIKit

class FavoriteShowsView: UIViewController {
    
    /*
     * MARK: Variables
     */
    var viewModel = FavoriteShowsViewModelList()
    
    
    /*
     * MARK: Outlets
     */
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print("Hola desde FavoriteShows")
        self.activity.isHidden = true
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.retriveLocalFavoriteShows()
        self.tableView.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    /*
     * Se dispara al activar el segue
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "FavoritesToDetail" {
            if let destino = segue.destination as? DetailShowView {
                destino.viewModel.setShow(show: self.viewModel.getSelectetShow()!)
            }
        }
    }

}

/*
 * MARK: Funciones Propias
 */
extension FavoriteShowsView {
    
    private func bind() {
        self.viewModel.refreshData = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.activity.stopAnimating()
                self?.activity.isHidden = true
            }
        }
    }
    
    private func configureView() {
        setupTitleBar()
        setupBottomBar()
    }
    
    private func setupBottomBar() {
        tabBarController?.tabBar.tintColor = UIColor(named: "titlebar")
    }
    
    private func setupTitleBar() {
        configureNavigationBar(largeTitleColor: .white, backgoundColor: UIColor(named: "titlebar")!, tintColor: .white, title: "Favorite Shows", preferredLargeTitle: true)
    }
    
}

/*
 * MARK: UITableViewDelegate, UITableViewDataSource
 */
extension FavoriteShowsView:UITableViewDelegate, UITableViewDataSource, FavoriteShowsCustomCellDelegator {
    
    func callSegueFromCell(show: Show) {
        self.viewModel.setSelectetShow(show: show)
        self.performSegue(withIdentifier: "FavoritesToDetail", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCellFavoriteShows") as! FavoriteShowsCustomCell
        
        cell.delegate = self
        cell.show = viewModel.dataArray[indexPath.row]
        cell.titleShow.text = viewModel.dataArray[indexPath.row].name
        
        let imagePath = viewModel.dataArray[indexPath.row].image.medium
        
        if let encoded = imagePath.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
           let url = URL(string: encoded) {
            cell.imageShow.kf.setImage(with: url)
        }
         
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .insert {
            // Accion de swipe
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
            // accion de eliminar
            self.viewModel.deleteFavoriteShow(row: indexPath.row)
            self.tableView.reloadData()
        }
        
        if #available(iOS 13.0, *) {
            deleteAction.title = "Delete"
        } else {
            // Fallback on earlier versions
        }
        
        deleteAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
}
