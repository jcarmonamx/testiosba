//
//  TvShowsViewModel.swift
//  TestIOSBA
//
//  Created by Manuel on 17/10/21.
//

import Foundation
import CoreData

class TvShowsViewModelList {
    
    // Modelo
    private var model = TvShowModel()
    
    // Capa de datos
    var networkManager: NetworkManager! = NetworkManager()
    
    // Mecanismo de enlace
    var refreshData = { () -> () in }
    
    // Fuente de datos
    var dataArray: [Show] = [] {
        didSet {
            refreshData()
        }
    }
    
    func retriveTvShowsFromWeb () {
        networkManager.getTvShows() { shows, error in
            self.dataArray = shows!
        }
    }
    
    func retriveTvShowsFromLocal () {
    
        // ESTO NO DEBERIA IR AQUI, FATO TIEMPO
        
        let context = Connection().context()
        
        let fetchRequestShows:NSFetchRequest<CDShows> = CDShows.fetchRequest()
        
        var cdShows : [CDShows] = []
        var shows = [Show]()
        
        do {
            cdShows = try context.fetch(fetchRequestShows)
        } catch let error {
            print("Erro al obtener los shows: \(error.localizedDescription)")
        }
        
        print("Entrando a retriveTvShowsFromLocal")
        
        cdShows.forEach({ (cdShow) in
            let fetchRequestImage:NSFetchRequest<CDImages> = CDImages.fetchRequest()
            
            let id = NSNumber(value: Int(cdShow.id))
            
            fetchRequestImage.predicate = NSPredicate(format: "show_id = %@", id as CVarArg)
            
            var image:Image?
            
            do {
                let cdImage = try context.fetch(fetchRequestImage).first
                image = Image(show_id: Int(cdShow.id), medium: cdImage!.medium ?? "", original: cdImage!.original ?? "")
            } catch let error {
                print("Erro al obtener la imagen: \(error.localizedDescription)")
            }
            
            let fetchRequestExternal:NSFetchRequest<CDExternals> = CDExternals.fetchRequest()
            
            fetchRequestExternal.predicate = NSPredicate(format: "show_id == %@", id as CVarArg)
            
            var external:Externals?
            
            do {
                let cdExternal = try context.fetch(fetchRequestExternal).first
                external = Externals(show_id: Int(cdShow.id), tvrage: Int(cdExternal!.tvrage), thetvdb: Int(cdExternal!.thetvdb), imdb: cdExternal!.imdb)
            } catch let error {
                print("Error al obtener el external: \(error.localizedDescription)")
            }
            
            let show:Show = Show(id: Int(cdShow.id), name: cdShow.name ?? "", url: cdShow.url ?? "", summary: cdShow.summary ?? "", externals: external!, image: image!)
            
            shows.append(show)
        })
        
        self.dataArray = shows
    }
    
    func deleteFavoriteShow(row: Int) {
        
        let show = self.dataArray[row]
        
        // ESTO NO DEBERIA IR AQUI, FATO TIEMPO
        let context = Connection().context()
        
        let fetchRequestShow:NSFetchRequest<CDShows> = CDShows.fetchRequest()
        
        let id = NSNumber(value: Int(show.id))
        
        fetchRequestShow.predicate = NSPredicate(format: "id = %@", id as CVarArg)
        
        let cdShows = try! context.fetch(fetchRequestShow)
        
        cdShows.forEach({ (cdShow) in
            context.delete(cdShow)
        })

        do {
            try context.save()
        } catch {
            // Do something... fatalerror
            print("Error al eliminar el show \(error.localizedDescription)")
        }
        
    }
    
    func saveFavoriteShow (row: Int) {
        
        let show = self.dataArray[row]
        
        print("llegue a saveFavoriteShow: \(show)")
        
        let context = Connection().context()
        
        let entityShow = NSEntityDescription.insertNewObject(forEntityName: "CDShows", into: context) as! CDShows
        
        entityShow.id = Int64(show.id)
        entityShow.name = show.name
        entityShow.url = show.url
        entityShow.summary = show.summary
        
        do {
            try context.save()
        } catch let error {
            print("No guardo el show: \(error.localizedDescription)")
        }
        
        print("Se guardo el show")
        
        // Guardamos la imagen
        let entityImage = NSEntityDescription.insertNewObject(forEntityName: "CDImages", into: context) as! CDImages
        
        entityImage.show_id = Int64(show.id)
        entityImage.medium = show.image.medium
        entityImage.original = show.image.original
        
        //entityShow.mutableSetValue(forKey: "relationshipToImages").add(entityImage)
        
        do {
            try context.save()
        } catch let error {
            print("No guardo la imagen: \(error.localizedDescription)")
        }
        
        print("Se guardo la imagen: \(entityImage)")
        
        // Guardamos el external
        let entityExternal = NSEntityDescription.insertNewObject(forEntityName: "CDExternals", into: context) as! CDExternals
        
        entityExternal.show_id = Int64(show.id)
        
        let _imdb = show.externals.imdb
        
        if _imdb != nil {
            entityExternal.imdb = _imdb
        }
        
        //entityShow.mutableSetValue(forKey: "relationshipToExternals").add(entityExternal)
        
        do {
            try context.save()
        } catch let error {
            print("No guardo el external: \(error.localizedDescription)")
        }
        
        print("Se guardo el external")
        
    }
    
    func isFavorite(row: Int) -> Bool{
        
        let show = self.dataArray[row]
        
        let context = Connection().context()
        
        let fetchRequestShow:NSFetchRequest<CDShows> = CDShows.fetchRequest()
        
        let id = NSNumber(value: Int(show.id))
        
        fetchRequestShow.predicate = NSPredicate(format: "id = %@", id as CVarArg)
        
        do {
            let shows = try context.fetch(fetchRequestShow)
            
            if shows.count > 0 {
                return true
            }
            
        } catch let error {
            print("Error al obtener los shows: \(error.localizedDescription)")
        }
        
        return false
    }
    
    func getSelectetShow() -> Show? {
        return model.selectedShow
    }
    
    func setSelectetShow(show:Show) {
        model.selectedShow = show
    }
    
    
}
