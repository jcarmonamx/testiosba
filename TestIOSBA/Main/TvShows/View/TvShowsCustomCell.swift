//
//  TvShowsCustomCell.swift
//  TestIOSBA
//
//  Created by Manuel on 17/10/21.
//

import UIKit

protocol TvShowsCustomCellDelegator {
    func callSegueFromCell(show:Show)
}

class TvShowsCustomCell: UITableViewCell {
    
    var delegate:TvShowsCustomCellDelegator!
    
    /*
     * MARK: Variables
     */
    
    var show:Show?
    
    /*
     * MARK: Outlets
     */
    
    @IBOutlet weak var imageShow: UIImageView!
    @IBOutlet weak var titleShow: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(seleccion)))
    }

}

/*
 * MARK: Funciones propias
 */
extension TvShowsCustomCell {
    // Al seleccionar un row
    @objc func seleccion () {
        guard let _show =  self.show, let _ = self.delegate else { return }
        self.delegate.callSegueFromCell(show: _show)
    }
}
