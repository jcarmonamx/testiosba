//
//  TvShowsView.swift
//  TestIOSBA
//
//  Created by Manuel on 17/10/21.
//

import UIKit
import Kingfisher

class TvShowsView: UIViewController {
    
    /*
     * MARK: Variables
     */
    var viewModel = TvShowsViewModelList()
    
    
    /*
     * MARK: Outlets
     */
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    
    /*
     * MARK: Fuinciones del sistema
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print("Hola desde TvShowsView")
        configureView()
        bind()
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    /*
     * Se dispara al activar el segue
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "TvShowsToDetail" {
            if let destino = segue.destination as? DetailShowView {
                destino.viewModel.setShow(show: self.viewModel.getSelectetShow()!)
            }
        }
    }

}

extension TvShowsView:UITableViewDelegate, UITableViewDataSource, TvShowsCustomCellDelegator {
    
    func callSegueFromCell(show: Show) {
        self.viewModel.setSelectetShow(show: show)
        self.performSegue(withIdentifier: "TvShowsToDetail", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCellTvShows") as! TvShowsCustomCell
        
        cell.delegate = self
        cell.show = viewModel.dataArray[indexPath.row]
        cell.titleShow.text = viewModel.dataArray[indexPath.row].name
        
        let imagePath = viewModel.dataArray[indexPath.row].image.medium
        
        if let encoded = imagePath.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
           let url = URL(string: encoded) {
            cell.imageShow.kf.setImage(with: url)
        }
         
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Action swipe
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
            // action delete
            if self.viewModel.isFavorite(row: indexPath.row) {
                self.viewModel.deleteFavoriteShow(row: indexPath.row)
            } else {
                self.viewModel.saveFavoriteShow(row: indexPath.row)
                self.tabBarController?.selectedIndex = 1
            }
            
            self.tableView.reloadData()
            
        }
        
        if #available(iOS 13.0, *) {
            
            if self.viewModel.isFavorite(row: indexPath.row) {
                deleteAction.title = "Delete"
            } else {
                deleteAction.title = "Favorite"
            }
            
        } else {
            // Fallback on earlier versions
        }
        
        if self.viewModel.isFavorite(row: indexPath.row) {
            deleteAction.backgroundColor = .red
        } else {
            deleteAction.backgroundColor = .green
        }
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
}

/*
 * MARK: Funciones propias
 */
extension TvShowsView {
    
    private func bind() {
        self.viewModel.refreshData = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.activity.stopAnimating()
                self?.activity.isHidden = true
            }
        }
    }
    
    private func configureView() {
        setupTitleBar()
        setupBottomBar()
        self.activity.isHidden = false
        self.activity.startAnimating()
        viewModel.retriveTvShowsFromLocal()
        viewModel.retriveTvShowsFromWeb()
        //self.activity.isHidden = true
        //self.activity.stopAnimating()
        
    }
    
    private func setupBottomBar() {
        tabBarController?.tabBar.tintColor = UIColor(named: "titlebar")
    }
    
    private func setupTitleBar() {
        configureNavigationBar(largeTitleColor: .white, backgoundColor: UIColor(named: "titlebar")!, tintColor: .white, title: "TV Shows", preferredLargeTitle: true)
    }
    
}
