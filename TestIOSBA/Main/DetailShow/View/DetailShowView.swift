//
//  DetailShowView.swift
//  TestIOSBA
//
//  Created by Manuel on 17/10/21.
//

import UIKit

class DetailShowView: UIViewController {
    
    /*
     * MARK: Variables
     */
    
    var viewModel = DetailShowViewModel()
    
    /*
     * MARK: Oulets
     */
    
    @IBOutlet weak var imageShow: UIImageView!
    @IBOutlet weak var summary: UITextView!
    
    @IBOutlet weak var buttonTvMaze: UIButton!
    
    @IBAction func buttonTvMazeAction(_ sender: UIButton) {
        if let encoded = self.viewModel.getShow()?.url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
           let url = URL(string: encoded) {
            if #available(iOS 10, *){
                UIApplication.shared.open(url)
            }else{
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBOutlet weak var buttonIMDb: UIButton!
    
    @IBAction func buttonIMDbAction(_ sender: UIButton) {
        if let encoded = self.viewModel.getIMDB().addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
           let url = URL(string: encoded) {
            if #available(iOS 10, *){
                UIApplication.shared.open(url)
            }else{
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    /*
     * MARK: Funciones del sistema
     */

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailShowView {
    
    func configureView() {
        
        // Do any additional setup after loading the view.
        let imagePath = self.viewModel.getShow()?.image.medium
        
        if let encoded = imagePath?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
           let url = URL(string: encoded) {
            self.imageShow.kf.setImage(with: url)
        }
        
        self.summary.attributedText = self.viewModel.getShow()?.summary.htmlToAttributedString
        
        self.navigationItem.title = self.viewModel.getShow()?.name
        
        self.buttonIMDb.isHidden = self.viewModel.getButtonIMDBHidden()
        
        let favoriteBarButton = UIBarButtonItem(title: viewModel.getLabelButton(), style: .done, target: self, action: #selector(doFavoriteButtonAction))
            self.navigationItem.rightBarButtonItem  = favoriteBarButton
        
    }
    
    @objc func doFavoriteButtonAction(){
        self.viewModel.doFavoriteButtonAction()
        self.configureView()
    }
}
