//
//  Show.swift
//  TestIOSBA
//
//  Created by Manuel on 16/10/21.
//

import Foundation

typealias Shows = [Show]

struct Show: Codable {
    let id: Int
    let name:String
    let url: String
    let summary: String
    let externals: Externals
    let image: Image
    
    /*let type, language: String
    let genres: [String]
    let status: String
    let runtime, averageRuntime: Int?
    let premiered, ended: String?
    let officialSite: String?
    let schedule: Schedule
    let rating: Rating
    let weight: Int
    let network: Network?
    let updated: Int
    let links: Links?*/

    enum CodingKeys: String, CodingKey {
        case id, url, name, externals, image, summary /*, type, language, genres, status, runtime, averageRuntime, premiered, ended, officialSite, schedule, rating, weight, network, updated
        case links = "_links"*/
    }
}

/*struct DataShow: Decodable {
      let data: [DTOShow]

      /// una función un poco tosca pero para que se comprenda un poco
      func fromDTO(dtoShows: [DTOShow]) -> [Show] {
        var addShow = [Show]()
        dtoShows.forEach { (show) in
            let new = Show(id: show.id, name: show.name)
            addShow.append(new)
        }
        return addShow
      }
 }

 struct DTOShow: Decodable {
    let id:Int
    let name: String
 }
*/
