//
//  Country.swift
//  TestIOSBA
//
//  Created by Manuel on 16/10/21.
//

import Foundation

struct Country: Codable {
    let name, code, timezone: String
}
