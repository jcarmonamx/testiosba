//
//  Schedule.swift
//  TestIOSBA
//
//  Created by Manuel on 16/10/21.
//

import Foundation

struct Schedule: Codable {
    let time: String
    let days: [String]
}
