//
//  Network.swift
//  TestIOSBA
//
//  Created by Manuel on 16/10/21.
//

import Foundation

struct Network: Codable {
    let id: Int
    let name: String
    let country: Country
}
