//
//  External.swift
//  TestIOSBA
//
//  Created by Manuel on 16/10/21.
//

import Foundation

struct Externals: Codable {
    let show_id: Int?
    let tvrage: Int
    let thetvdb: Int?
    let imdb: String?
}
