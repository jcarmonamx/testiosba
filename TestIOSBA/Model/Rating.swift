//
//  Rating.swift
//  TestIOSBA
//
//  Created by Manuel on 16/10/21.
//

import Foundation

struct Rating: Codable {
    let average: Double?
}
