//
//  Image.swift
//  TestIOSBA
//
//  Created by Manuel on 16/10/21.
//

import Foundation

struct Image: Codable {
    let show_id: Int?
    let medium: String
    let original: String
}
