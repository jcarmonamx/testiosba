//
//  Connection.swift
//  TestIOSBA
//
//  Created by Manuel on 17/10/21.
//

import Foundation
import CoreData
import UIKit

class Connection {
    
    func context() -> NSManagedObjectContext {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        return delegate.persistentContainer.viewContext
    }
    
}
